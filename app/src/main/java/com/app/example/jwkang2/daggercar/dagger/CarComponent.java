package com.app.example.jwkang2.daggercar.dagger;

import com.app.example.jwkang2.daggercar.model.Car;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Component;

@Component( modules = {PartsModule.class, DieselEngineModule.class})
public interface CarComponent {
    Car getCar();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder horsePower(@Named("hoarse_power") int horsePower);

        @BindsInstance
        Builder capacity(@Named("capacity") int capacity);

        CarComponent build();
    }
}
