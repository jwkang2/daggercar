package com.app.example.jwkang2.daggercar.dagger;

import com.app.example.jwkang2.daggercar.model.parts.Block;
import com.app.example.jwkang2.daggercar.model.parts.Cylinders;
import com.app.example.jwkang2.daggercar.model.parts.Engine;
import com.app.example.jwkang2.daggercar.model.parts.SparkPlugs;
import com.app.example.jwkang2.daggercar.model.parts.engine.DieselEngine;

import javax.inject.Inject;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Module;
import dagger.Provides;

@Module
public class DieselEngineModule {
    @Provides
    Engine provideDieselEngine(DieselEngine dieselEngine) {
        return dieselEngine;
    }
}
