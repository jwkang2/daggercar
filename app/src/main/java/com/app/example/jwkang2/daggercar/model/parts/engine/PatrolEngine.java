package com.app.example.jwkang2.daggercar.model.parts.engine;

import android.util.Log;

import com.app.example.jwkang2.daggercar.model.parts.Block;
import com.app.example.jwkang2.daggercar.model.parts.Cylinders;
import com.app.example.jwkang2.daggercar.model.parts.Engine;
import com.app.example.jwkang2.daggercar.model.parts.SparkPlugs;

import javax.inject.Inject;

public class PatrolEngine implements Engine {
    private static final String TAG = PatrolEngine.class.getSimpleName();
    private Block block;
    private Cylinders cylinders;
    private SparkPlugs sparkPlugs;

    @Inject
    public PatrolEngine(Block block, Cylinders cylinders, SparkPlugs sparkPlugs) {
        this.block = block;
        this.cylinders = cylinders;
        this.sparkPlugs = sparkPlugs;
    }

    @Override
    public void drive() {
        Log.i(TAG, "drive: PatrolEngine start");
    }
}
