package com.app.example.jwkang2.daggercar.model;

import com.app.example.jwkang2.daggercar.model.parts.Engine;
import com.jwkang2.wheels.Wheels;

import javax.inject.Inject;

public class Car {
    private Engine mEngine;
    @Inject
    public Car(Engine engine, Wheels wheels) {
        mEngine = engine;
    }

    public void start() {
        mEngine.drive();
    }
}
