package com.app.example.jwkang2.daggercar.dagger;

import com.app.example.jwkang2.daggercar.model.parts.Engine;
import com.app.example.jwkang2.daggercar.model.parts.engine.DieselEngine;
import com.app.example.jwkang2.daggercar.model.parts.engine.PatrolEngine;

import dagger.Module;
import dagger.Provides;

@Module
public class PatrolEngineModule {
    @Provides
    static Engine providePatrolEngine(PatrolEngine dieselEngine) {
        return dieselEngine;
    }
}
