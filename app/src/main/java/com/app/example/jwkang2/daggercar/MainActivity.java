package com.app.example.jwkang2.daggercar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.example.jwkang2.daggercar.dagger.CarComponent;
import com.app.example.jwkang2.daggercar.dagger.DaggerCarComponent;
import com.app.example.jwkang2.daggercar.dagger.DieselEngineModule;
import com.app.example.jwkang2.daggercar.model.Car;

public class MainActivity extends AppCompatActivity {
    Car car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CarComponent carComponent = DaggerCarComponent.builder()
                .horsePower(100)
                .capacity(200)
                .build();
        car = carComponent.getCar();
        car.start();
    }
}
