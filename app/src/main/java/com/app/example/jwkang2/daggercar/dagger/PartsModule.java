package com.app.example.jwkang2.daggercar.dagger;

import com.jwkang2.wheels.Rims;
import com.jwkang2.wheels.Tires;
import com.jwkang2.wheels.Wheels;

import dagger.Module;
import dagger.Provides;

@Module
public class PartsModule {
    @Provides
    static Tires provideTires() {
        return new Tires();
    }

    @Provides
    static Rims provideRims() {
        return new Rims();
    }

    @Provides
    static Wheels provideWheels(Tires tires, Rims rims) {
        return new Wheels(tires, rims);
    }
}
