package com.app.example.jwkang2.daggercar.model.parts;

public interface Engine {
    void drive();
}
