package com.app.example.jwkang2.daggercar.model.parts.engine;

import android.util.Log;

import com.app.example.jwkang2.daggercar.model.parts.Block;
import com.app.example.jwkang2.daggercar.model.parts.Cylinders;
import com.app.example.jwkang2.daggercar.model.parts.Engine;
import com.app.example.jwkang2.daggercar.model.parts.SparkPlugs;

import javax.inject.Inject;
import javax.inject.Named;

public class DieselEngine implements Engine {
    private static final String TAG = DieselEngine.class.getSimpleName();
    private Block block;
    private Cylinders cylinders;
    private SparkPlugs sparkPlugs;
    private int hoarsePower;
    private int capacity;

    @Inject
    public DieselEngine(Block block, Cylinders cylinders, SparkPlugs sparkPlugs, @Named("hoarse_power") int hoarsePower, @Named("capacity") int capacity) {
        this.block = block;
        this.cylinders = cylinders;
        this.sparkPlugs = sparkPlugs;
        this.hoarsePower = hoarsePower;
        this.capacity = capacity;
    }

    @Override
    public void drive() {
        Log.i(TAG, "drive: DieselEngine start. hoarsePower => " + hoarsePower + " capacity => " + capacity);
    }
}
